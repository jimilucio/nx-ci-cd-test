stages:          # List of stages for jobs, and their order of execution
    - install
    - lint
    - build
    - test
    - security

variables:
  # Docker driver: speed things up: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/21374
  DOCKER_DRIVER: overlay
  # Avoid fetching everything
  GIT_DEPTH: 0



image: tarampampam/node:13-alpine

before_script:
  - date
  - echo "Pipeline ID = $CI_PIPELINE_ID"
  - echo "Project name = $CI_PROJECT_NAME"
  - echo "Build ref = $CI_BUILD_REF_NAME"
  - if [ "$CI_MERGE_REQUEST_ID" != "" ]; then IS_PR=true; else IS_PR=false; fi
  - echo $IS_PR
  - pwd
  - echo "Default environment:"
  - env
  # TODO track the following issue for performance improvements https://gitlab.com/gitlab-org/gitlab-runner/issues/1797
  # Current approach: install everything based on cache for each job
  - echo "Installing dependencies (if needed)"
  # The NPM_CACHE_FOLDER env variable is defined in the App's CI dockerfile
  - if [[ ! -d node_modules ]] || [[ -n `git diff --name-only origin/master HEAD | grep "\package-lock.json\b"` ]]; then npm ci --cache ${NPM_CACHE_FOLDER} --prefer-offline --no-audit --no-optional; fi
  - export COMMANDS=$(node ./tools/scripts/calculate-commands.js $IS_PR)
  - echo $COMMANDS
  - date

after_script:
  - date


Lint:
  stage: test
  when: always
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
    policy: pull
  variables:
    COMMANDS: $[ dependencies.initial_setup.outputs['COMMANDS'] ]
  script:
    - echo $COMMANDS
    - node ./tools/scripts/run-many.js '$COMMANDS' lint1 lint

Unit tests:
  stage: test
  when: always
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run affected:e2e -- --base=remotes/origin/master --head=HEAD --parallel
  # Help Gitlab extract coverage data
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    name: "Coverage reports"
    # expire_in: 3 days # default: 30 days
    paths: # TODO complete
      - coverage/ # keep code coverage reports


End-to-end tests:
  stage: test
  when: manual
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
    policy: pull
  allow_failure: true # Setting this to false would make this job mandatory
  script:
    - npm run affected:e2e -- --headless --base=remotes/origin/master --head=HEAD --parallel

Static security audit:
  stage: security
  when: always
  script:
    - npm audit --audit-level critical

Build affected:
  stage: build
  when: always
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run affected:build -- --base=remotes/origin/master --head=HEAD # We always consider the master
  artifacts:
    name: "Build"
    #expire_in: 1 days # default: 30 days
    paths:
      - dist/ # keep the built application

Build all prod:
  stage: build
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run build:all
  artifacts:
    name: "Production build"
    #expire_in: 15 days # default: 30 days
    paths:
      - dist/ # keep the built application
