import { Component } from '@angular/core';

@Component({
  selector: 'nx-ci-cd-testing-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'landing';
}
